package ca.claurendeau.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import ca.claurendeau.domaine.Livre;
import ca.claurendeau.repos.LivreRepository;

@RestController
@CrossOrigin
public class FinalController {
    
    @Autowired
    private LivreRepository livreRepository;

    @GetMapping("/savelivre/{id}/{titre}/{autheur}")
    public Livre saveBook(@PathVariable Long id, @PathVariable String titre, @PathVariable String autheur) {
        // VOTRE CODE ICI
        return null;
    }
    
    @GetMapping("/loadlivres")
    public List<Livre> loadLivres() {
        // VOTRE CODE ICI
        return null;
    }
}
